stages:
  - siril

## GNU/Linux 64-bit CIs ##
 
.siril-debian/testing-base:
  image: debian:testing
  stage: siril
  variables:
    GIT_DEPTH: "1"
    INSTALL_DIR: "_install"
    INSTALL_PREFIX: "${CI_PROJECT_DIR}/${INSTALL_DIR}"
    GIT_SUBMODULE_STRATEGY: recursive
  artifacts:
    expire_in: 1 week
    when: always
    name: "app-build-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
    paths:
    - "${INSTALL_DIR}"
    - _build
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends
        build-essential
        cmake
        desktop-file-utils
        hicolor-icon-theme
        git
        intltool
        libexiv2-dev
        libgtk-3-dev
        libcfitsio-dev
        libfftw3-dev
        libgsl-dev
        libconfig-dev
        libopencv-dev
        librsvg2-dev
        libraw-dev
        libffms2-dev
        libtiff-dev
        libjpeg-dev
        libheif-dev
        libpng-dev
        libavformat-dev
        libavutil-dev
        libavcodec-dev
        libswscale-dev
        libswresample-dev
        libcurl4-gnutls-dev
    - export PKG_CONFIG_PATH="${INSTALL_PREFIX}/lib/pkgconfig:${INSTALL_PREFIX}/share/pkgconfig"
    - export PKG_CONFIG_PATH="${INSTALL_PREFIX}/lib/`gcc -print-multiarch`/pkgconfig/:$PKG_CONFIG_PATH"
    - export PKG_CONFIG_PATH="${INSTALL_PREFIX}/share/`gcc -print-multiarch`/pkgconfig/:$PKG_CONFIG_PATH"
    - export LD_LIBRARY_PATH="${INSTALL_PREFIX}/lib:${LD_LIBRARY_PATH}"
    - export LD_LIBRARY_PATH="${INSTALL_PREFIX}/lib/`gcc -print-multiarch`:$LD_LIBRARY_PATH"
    - export XDG_DATA_DIRS="${INSTALL_PREFIX}/share:/usr/local/share:/usr/share"
    - export PATH="${INSTALL_PREFIX}/bin:$PATH"

siril-debian/testing-autotools:
  extends: .siril-debian/testing-base
  script:
    - NOCONFIGURE=1 ./autogen.sh
    - mkdir _build && cd _build
    - ../configure --prefix=${INSTALL_PREFIX}
    - make -j "$(nproc)" all install
    # - make check
    
## Windows 64-bit CIs ##

siril-win64:
  image: fedora:31
  stage: siril
  variables:
    GIT_DEPTH: "1"
    XDG_CACHE_HOME: "$CI_PROJECT_DIR/.cache/"
    XDG_DATA_HOME:  "$CI_PROJECT_DIR/.local/share/"
  cache:
    paths:
    - .cache/crossroad/
    - dnf_cache
  before_script:
    - dnf install --assumeyes --setopt=cachedir=`pwd`/dnf_cache --verbose
        automake
        autoconf
        cmake
        gettext
        git
        intltool
        libtool
        libxslt
        make
        python3
        mingw64-gcc
        mingw64-gcc-c++
        mingw64-binutils
        mingw64-libgomp
        cpio rpm
        python3-docutils
        python3-setuptools
        shared-mime-info
        which
    - git clone --depth=${GIT_DEPTH} git://git.tuxfamily.org/gitroot/crossroad/crossroad.git
    - cd crossroad
    - ./setup.py install --prefix=`pwd`/../.local
    - cd ..
    - git submodule sync --recursive
    - git submodule update --init --recursive
  script:
    - export PATH="`pwd`/.local/bin:$PATH"
    # Pre-built dependencies
    - echo 'crossroad source msys2' | crossroad w64 siril --run="-"
    - echo 'crossroad install fftw cfitsio exiv2 gtk3 libconfig gsl opencv' | crossroad w64 siril --run="-"
    # librtprocess
    - mkdir deps/librtprocess/_build && cd deps/librtprocess/_build
    - echo 'crossroad cmake .. && make && make install' | crossroad w64 siril --run="-"
    ## Build Siril ##
    - cd ../../.. && mkdir _build && cd _build
    - echo 'crossroad ../configure && make && make install &&
            cp -fr $CROSSROAD_PREFIX/ ../siril/' | crossroad w64 siril --run="-"
  artifacts:
    name: "${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
    when: always
    expire_in: 1 week
    paths:
      - _build/
      - deps/librtprocess/_build
      - siril/
    
flatpak:
  # Update the image URL whenever the runtime version in the flatpak manifest is changed
  image: 'registry.gitlab.gnome.org/gnome/gnome-runtime-images/gnome:3.36'
  stage: 'siril'
  variables:
    MANIFEST_PATH: "build/flatpak/org.free_astro.siril.json"
    FLATPAK_MODULE: "siril"
    APP_ID: "org.free_astro.siril"
    RUNTIME_REPO: "https://flathub.org/repo/flathub.flatpakrepo"
    BUNDLE: "org.free_astro.siril.flatpak"
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - flatpak-builder --stop-at=${FLATPAK_MODULE} flatpak_app ${MANIFEST_PATH} 1>/dev/null
    # Make sure to keep this in sync with the Flatpak manifest, all arguments
    # are passed except the config-args because we build it ourselves
    - flatpak build flatpak_app ./autogen.sh --prefix=/app
    - flatpak build flatpak_app make
    - flatpak build flatpak_app make install
    - flatpak-builder --finish-only --repo=repo ${BRANCH:+--default-branch=$BRANCH} flatpak_app ${MANIFEST_PATH}
    # TODO: Run automatic tests inside the Flatpak env 
    # - >
    #   xvfb-run -a -s "-screen 0 1024x768x24"
    #   flatpak build
    #   --env=LANG=C.UTF-8
    #   --env=NO_AT_BRIDGE=1
    #   ${TEST_BUILD_ARGS}
    #   flatpak_app
    #   dbus-run-session
    #   meson test -C _build --no-stdsplit --print-errorlogs ${TEST_RUN_ARGS}

    # Generate a Flatpak bundle
    - flatpak build-bundle repo ${BUNDLE} --runtime-repo=${RUNTIME_REPO} ${APP_ID} ${BRANCH}
  artifacts:
    name: 'Flatpak artifacts'
    expose_as: 'Get Flatpak bundle here'
    when: 'always'
    paths:
      - "${BUNDLE}"
      - 'repo/'
    expire_in: 1 week
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - '.flatpak-builder/downloads'
      - '.flatpak-builder/git'